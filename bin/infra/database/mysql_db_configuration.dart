import 'package:mysql1/mysql1.dart';

import '../../utils/custom_env.dart';
import 'db_configuration.dart';

class MySqlDBConfiguration implements DBConfiguration {
  MySqlConnection? _connection;

  @override
  Future<MySqlConnection> get connection async {
    if (_connection == null) _connection = await createConnection();
    if (_connection == null)
      throw Exception('[ERROR/DB] -> Failed Create Connection');
    return _connection!;
  }

  @override
  Future<MySqlConnection> createConnection() async =>
      /*await MySqlConnection.connect(
        ConnectionSettings(
            port: 3306,
            host: "localhost",
            user: 'dart_user',
            db: 'dart',
            password: 'dart_pass'),
      );*/
      await MySqlConnection.connect(
        ConnectionSettings(
            port: 3306,
            host: "sql.freedb.tech",
            user: 'freedb_teste_Everton',
            db: 'freedb_shopfoods',
            password: 'rf&xyD#sKCuK*r4'),
      );

  @override
  execQuery(String sql, [List? params]) async {
    var conn = await this.connection;
    return await conn.query(sql, params);
  }
}
