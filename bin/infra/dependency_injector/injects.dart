import '../../apis/categoria_api.dart';
import '../../apis/lista_comras.api.dart';
import '../../apis/noticias_api.dart';
import '../../apis/login_api.dart';
import '../../apis/produto_api.dart';
import '../../apis/usuario2_api.dart';
import '../../apis/usuario_api.dart';
import '../../dao/categoria_dao.dart';
import '../../dao/lista_compras_dao.dart';
import '../../dao/noticia_dao.dart';
import '../../dao/produto_dao.dart';
import '../../dao/usuario2_dao.dart';
import '../../dao/usuario_dao.dart';
import '../../models/categoria_model.dart';
import '../../models/lista_compras_model.dart';
import '../../models/noticia_model.dart';
import '../../models/produto_model.dart';
import '../../models/usuario2_model.dart';
import '../../services/categoria_service.dart';
import '../../services/generic_service.dart';
import '../../services/lista_compras_service.dart';
import '../../services/login_service.dart';
import '../../services/noticia_service.dart';
import '../../services/produto_service.dart';
import '../../services/usuario2_service.dart';
import '../../services/usuario_service.dart';
import '../database/db_configuration.dart';
import '../database/mysql_db_configuration.dart';
import '../security/security_service.dart';
import '../security/security_service_imp.dart';
import 'dependency_injector.dart';

class Injects {
  static DependencyInjector initialize() {
    var di = DependencyInjector();

    di.register<DBConfiguration>(() => MySqlDBConfiguration());
    di.register<SecurityService>(() => SecurityServiceImp());
    //lista de compras
    di.register<ListaComprasDAO>(() => ListaComprasDAO(di<DBConfiguration>()));
    di.register<GenericService<ListaCompras>>(
      () => ListaComprasService(di<ListaComprasDAO>()),
    );
    di.register<ListaComprasAPI>(
      () => ListaComprasAPI(di<GenericService<ListaCompras>>()),
    );
    //categoria
     di.register<CategoriaDAO>(() => CategoriaDAO(di<DBConfiguration>()));
    di.register<GenericService<Categoria>>(
      () => CategoriaService(di<CategoriaDAO>()),
    );
    di.register<CategoriaAPi>(
      () => CategoriaAPi(di<GenericService<Categoria>>()),
    );
    
    //produto
    di.register<ProdutoDAO>(() => ProdutoDAO(di<DBConfiguration>()));
    di.register<GenericService<Produto>>(
      () => ProdutoService(di<ProdutoDAO>()),
    );
    di.register<ProdutoAPi>(
      () => ProdutoAPi(di<GenericService<Produto>>()),
    );
    // noticias
    di.register<NoticiaDAO>(() => NoticiaDAO(di<DBConfiguration>()));
    di.register<GenericService<NoticiaModel>>(
      () => NoticiaService(di<NoticiaDAO>()),
    );
    di.register<NoticiasApi>(
      () => NoticiasApi(di<GenericService<NoticiaModel>>()),
    );

    // usuario
    di.register<UsuarioDAO>(() => UsuarioDAO(di<DBConfiguration>()));
    di.register<UsuarioService>(() => UsuarioService(di<UsuarioDAO>()));
    di.register<UsuarioApi>(() => UsuarioApi(di<UsuarioService>()));
    //teste
    //usuario2
     di.register<Usuario2DAO>(() => Usuario2DAO(di<DBConfiguration>()));
   di.register<Usuario2Service>(() => Usuario2Service(di<Usuario2DAO>()));
    di.register<Usuario2Api>(() => Usuario2Api(di<Usuario2Service>()));
    // login
    di.register<LoginService>(() => LoginService(di<Usuario2Service>()));
    di.register<LoginApi>(
      () => LoginApi(di<SecurityService>(), di<LoginService>()),
    );

    return di;
  }
}
