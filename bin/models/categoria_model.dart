class Categoria {
  int? Id;
  String? Nome;
  String? DataCadastro;
  int? IdUsuario;

  Categoria();

  Categoria.create({
    this.Id,
    this.Nome,
    this.DataCadastro,
    this.IdUsuario,
  });

  /*factory Categoria.fromMap(Map map) {
    return Categoria()
    ..Id=map['Id']?.toInt()
    ..Nome= map['Nome']
    ..DataCadastro=map['DataCadastro']
    ..IdUsuario=map['IdUsuario'];
    }*/

    factory Categoria.fromMap(Map<String, dynamic> map) {
    
    
    return Categoria.create(
        Id: int.parse(map['Id'].toString()),
        IdUsuario: int.parse(map['IdUsuario'].toString()),
        Nome: map['Nome'].toString(),
        DataCadastro: map['DataCadastro'].toString()
        );
  }

    Map toJson() {
    return {
      'id': Id,
      'Nome': Nome,
      'DataCadastro': DataCadastro,
      'IdUsuario': IdUsuario
    };
  }
}
