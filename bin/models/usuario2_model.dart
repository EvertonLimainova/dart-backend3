class Usuario2 {
  int? Id;
  String? role;
  String? email;
  String? nome;
  String? token;
  String? pass;

  Usuario2();

  Usuario2.create({
    this.Id,
    this.role,
    this.email,
    this.nome,
    this.token,
    this.pass,
  });

  /*factory Usuario2.fromEmail(Map map) {
    print(
        'mapa: ${map['Id'].toString()} nome: ${map['nome']} senha:${map['pass']}');
    return Usuario2()
      ..Id = map['Id']?.toInt()
      ..nome = map['nome']
      ..email = map['email']
      ..pass = map['pass'];
  }*/
/*factory Produto.fromMap(Map<String, dynamic> map) {
    return Produto.create(
      Id:int.parse(map['Id'].toString()),
      CategoriaId: int.parse(map['CategoriaId'].toString()),
      DataCompra: map['DataCompra'].toString(),
      Descricao: map['Descricao'].toString(),
      Peresivel: map['Peresivel'].toString(),
      Preco: double.parse(['Preco'].toString()),
      Quantidade: int.parse(['Quantidade'].toString()),
      UrlProduto: map['UrlProduto'].toString(),
      UsuarioId: int.parse(['UsuarioId'].toString()),
      Validate: map['Validate'].toString()
    );
  }*/

  factory Usuario2.fromEmail(Map map) {
    return Usuario2.create(    
      Id: int.parse(map['Id'].toString()),
      nome: map['nome'].toString(),
      email: map['email'].toString(),
      pass: map['pass'].toString()
    );
  }
  factory Usuario2.fromRequest(Map map) {
    return Usuario2.create(
        Id: int.parse(map['Id'].toString()),
        email: map['email'].toString(),
        nome: map['nome'].toString(),
        pass: map['pass'].toString(),
        role: map['role'].toString(),
        token: map['token'].toString());
  }
  /*factory Usuario2.fromRequest(Map map) {
    return Usuario2()
      ..Id = map["Id"]
      ..nome = map['nome']
      ..email = map['email']
      ..role = map["role"]
      ..pass = map['pass'];
  }*/

  Map toJson() {
    return {'id': Id, 'nome': nome, 'email': email, 'role': role, 'pass': pass};
  }

  @override
  String toString() {
    return 'Usuario2(id: $Id, name: $nome, email: $email)';
  }
}
