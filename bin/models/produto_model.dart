class Produto {
  int? Id;
  String? Descricao;
  int? Quantidade;
  double? Preco;
  String? DataCompra;
  String? UrlProduto;
  int? CategoriaId;
  int? UsuarioId;
  String? Validate;
  String? Peresivel;

  Produto();
  Produto.create({
    this.Id,
    this.Descricao,
    this.Quantidade,
    this.Preco,
    this.DataCompra,
    this.UrlProduto,
    this.CategoriaId,
    this.UsuarioId,
    this.Validate,
    this.Peresivel,
  });

  factory Produto.fromMap(Map<String, dynamic> map) {
    print('preco: ${map['Preco'].toString()}');
    print('quantidade: ${map['Quantidade'].toString()}');
    return Produto.create(
        Id: int.parse(map['Id'].toString()),
        CategoriaId: int.parse(map['CategoriaId'].toString()),
        DataCompra: map['DataCompra'].toString(),
        Descricao: map['Descricao'].toString(),
        Peresivel: map['Peresivel'].toString(),
        Preco: double.parse(map['Preco'].toString()),
        Quantidade: int.parse(map['Quantidade'].toString()),
        UrlProduto: map['UrlProduto'].toString(),
        UsuarioId: int.parse(map['UsuarioId'].toString()),
        Validate: map['Validate'].toString());
  }
  /*factory Produto.fromMap(Map map) {
    
    return Produto().Id = map['id']?.toInt
      ..Descricao = map['Descricao'] ?? ''
      ..Quantidade = map['Quantidade'].toString()
      ..Preco = double.parse(map['Preco'].toString())
      ..DataCompra = map['DataCompra'].toString()
      ..UrlProduto = map['UrlProduto'].toString()
      ..CategoriaId = map['CategoriaId']?.toInt()
      ..UsuarioId = map['UsuarioId']?.toInt()
      ..Validate = map['Validate'].toString()
      ..Peresivel = map['Peresivel'].toString();
  }*/

  Map toJson() {
    return {
      'id': Id,
      'Descricao': Descricao,
      'Quantidade': Quantidade,
      'Preco': Preco,
      'DataCompra': DataCompra,
      'UrlProduto': UrlProduto,
      'CategoriaId': CategoriaId,
      'UsuarioId': UsuarioId,
      'Validate': Validate,
      'Peresivel': Peresivel
    };
  }

  @override
  String toString() {
    return 'Produto(id: $Id, Descricao: $Descricao, Quantidade: $Quantidade, Preco: $Preco, DataCompra: $DataCompra, UrlProduto: $UrlProduto,CategoriaId:$CategoriaId,UsuarioId:$UsuarioId, Validate:$Validate,Peresivel: $Peresivel)';
  }
}
