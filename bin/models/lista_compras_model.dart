class ListaCompras {
  int? Id;
  double? Total;
  String? dataLista;
  int? UsuarioId;
  int? ProdutoId;

  ListaCompras();

  ListaCompras.create({
    this.Id,
    this.ProdutoId,
    this.Total,
    this.UsuarioId,
    this.dataLista,
  });

  /*factory ListaCompras.fromMap(Map map) {
    return ListaCompras().Id = map['id']?.toInt()
      ..Total = map['Descricao'].toString()
      ..dataLista = map['Quantidade'].toString()
      ..UsuarioId = double.parse(map['Preco'].toString())
      ..ProdutoId = map['DataCompra'].toString();
  }*/

  factory ListaCompras.fromMap(Map map) {
    return ListaCompras.create(
        Id: int.parse(map['Id'].toString()),
        ProdutoId: int.parse(map['ProdutoId'].toString()),
        UsuarioId: int.parse(map['UsuarioId'].toString()),
        dataLista: map['dataLista'].toString(),
        Total: double.parse(map['Total'].toString()));
  }

  Map toJson() {
    return {
      'Id': Id,      
      'ProdutoId': ProdutoId,
      'UsuarioId': UsuarioId,
      'dataLista': dataLista,
      'Total': Total,
    };
  }
}
