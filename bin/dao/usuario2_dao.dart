import '../infra/database/db_configuration.dart';
import '../models/usuario2_model.dart';
import 'dao.dart';

class Usuario2DAO implements DAO<Usuario2> {
  final DBConfiguration _dbConfiguration;
  Usuario2DAO(this._dbConfiguration);

  @override
  Future<bool> create(Usuario2 value) async {
    var result = await _dbConfiguration.execQuery(
      'insert into Usuario(email,nome,token,pass,role) values (?, ?, ?, ?, ?)',
      [value.email, value.nome, value.token, value.pass, value.role],
    );
    return result.affectedRows > 0;
  }

  @override
  Future<bool> delete(int id) async {
    var result = await _dbConfiguration
        .execQuery('DELETE from Usuario where id = ?', [id]);
    return result.affectedRows > 0;
  }

  @override
  Future<List<Usuario2>> findAll() async {
    var result = await _dbConfiguration.execQuery('SELECT * FROM Usuario');
    return result
        .map((r) => Usuario2.fromRequest(r.fields))
        .toList()
        .cast<Usuario2>();
  }

  @override
  Future<Usuario2?> findOne(int id) async {
    var result = await _dbConfiguration
        .execQuery('SELECT * FROM Usuario where Id = ?', [id]);
    return result.isEmpty ? null : Usuario2.fromRequest(result.first.fields);
  }

//
  @override
  Future<bool> update(Usuario2 value) async {
    var result = await _dbConfiguration.execQuery(
      'update Usuario set email=?, nome=?,token=?,pass=?, role=? where id=?',
      [value.email, value.nome, value.token, value.pass, value.role, value.Id],
    );
    return result.affectedRows > 0;
  }

  Future<Usuario2?> findByEmail(String email) async {
    print('email: ${email}');
    var r = await _dbConfiguration
        .execQuery('select * from Usuario where email = ?', [email]);
    print('r:  ${r.toString()}');
    return r.affectedRows == 0 ? null : Usuario2.fromEmail(r.first.fields);
  }
}
