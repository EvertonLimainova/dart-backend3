import '../infra/database/db_configuration.dart';
import '../models/categoria_model.dart';
import 'dao.dart';

class CategoriaDAO implements DAO<Categoria> {
  final DBConfiguration _dbConfiguration;
  CategoriaDAO(this._dbConfiguration);

  @override
  Future<bool> create(Categoria value) async {
    var result = await _dbConfiguration.execQuery(
      'insert into Categoria (Nome,DataCadastro,IdUsuario) values(?,?,?)',
      [value.Nome, value.DataCadastro, value.IdUsuario],
    );
    return result.affectedRows > 0;
  }

  @override
  Future<bool> delete(int id) async {
    var result = await _dbConfiguration
        .execQuery('DELETE from Categoria where id = ?', [id]);
    return result.affectedRows > 0;
  }

  @override
  Future<List<Categoria>> findAll() async {
    print('entrou aqui na categoria');
    var result = await _dbConfiguration.execQuery('SELECT * FROM Categoria');
    print(
        '${result.map((r) => Categoria.fromMap(r.fields)).toList().cast<Categoria>()}');
    return result
        .map((r) => Categoria.fromMap(r.fields))
        .toList()
        .cast<Categoria>();
  }

  @override
  Future<Categoria?> findOne(int id) async {
    var result = await _dbConfiguration
        .execQuery('SELECT * FROM Categoria where Id = ?', [id]);
    return result.isEmpty ? null : Categoria.fromMap(result.first.fields);
  }

//
  @override
  Future<bool> update(Categoria value) async {
    var result = await _dbConfiguration.execQuery(
      'update Categoria set Nome=?,DataCadastro=?,IdUsuario=? where Id=?',
      [value.Nome, value.DataCadastro, value.IdUsuario, value.Id],
    );
    return result.affectedRows > 0;
  }
}
