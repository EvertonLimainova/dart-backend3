import '../infra/database/db_configuration.dart';
import '../models/produto_model.dart';
import 'dao.dart';

class ProdutoDAO implements DAO<Produto> {
  final DBConfiguration _dbConfiguration;
  ProdutoDAO(this._dbConfiguration);

  @override
  Future<bool> create(Produto value) async {
    var result = await _dbConfiguration.execQuery(
      'INSERT INTO Produto (Id,Descricao,Quantidade,Preco,DataCompra,UrlProduto,CategoriaId,UsuarioId,Validade,Peresivel) VALUES (?,?,?,?,?,?,?,?,?,?)',
      [value.Id, value.Descricao, value.Quantidade,value.Preco,value.DataCompra,value.UrlProduto,value.CategoriaId,value.UsuarioId,value.Validate,value.Peresivel],
    );
    return result.affectedRows > 0;
  }

  @override
  Future<bool> delete(int id) async {
    var result = await _dbConfiguration
        .execQuery('DELETE from Produto where id = ?', [id]);
    return result.affectedRows > 0;
  }

  @override
  Future<List<Produto>> findAll() async {
    var result = await _dbConfiguration.execQuery('SELECT * FROM Produto');
    return result
        .map((r) => Produto.fromMap(r.fields))
        .toList()
        .cast<Produto>();
  }

  @override
  Future<Produto?> findOne(int id) async {
    var result = await _dbConfiguration
        .execQuery('SELECT * FROM Produto where Id = ?', [id]);
    return result.isEmpty ? null : Produto.fromMap(result.first.fields);
  }
//
  @override
  Future<bool> update(Produto value) async {
    var result = await _dbConfiguration.execQuery(
      'Update Produto Descricao=?,Quantidade=?,Preco=?,DataCompra=?,UrlProduto=?,CategoriaId=?,UsuarioId=?,Validade=?,Peresivel=? where Id=?',
      [value.Descricao, value.Quantidade, value.Preco,value.DataCompra,value.UrlProduto,value.CategoriaId,value.UsuarioId,value.Validate,value.Peresivel,value.Id],
    );
    return result.affectedRows > 0;
  }
}
