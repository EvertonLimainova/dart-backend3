import '../infra/database/db_configuration.dart';
import '../models/lista_compras_model.dart';
import 'dao.dart';

class ListaComprasDAO implements DAO<ListaCompras> {
  final DBConfiguration _dbConfiguration;
  ListaComprasDAO(this._dbConfiguration);

  @override
  Future<bool> create(ListaCompras value) async {
    print("mapeamento: ${value.toJson()}");
    var result = await _dbConfiguration.execQuery(
      'insert into ListaCompras (Total,dataLista,UsuarioId,ProdutoId) values (?,?,?,?)',
      [value.Total, value.dataLista, value.UsuarioId, value.ProdutoId],
    );
    return result.affectedRows > 0;
  }

  @override
  Future<bool> delete(int id) async {
    var result = await _dbConfiguration
        .execQuery('DELETE from ListaCompras where id = ?', [id]);
    return result.affectedRows > 0;
  }

  @override
  Future<List<ListaCompras>> findAll() async {
    var result = await _dbConfiguration.execQuery('SELECT * FROM ListaCompras');
    print('valor do resultado: ${result.toString()}');
    return result
        .map((r) => ListaCompras.fromMap(r.fields))
        .toList()
        .cast<ListaCompras>();
  }

  @override
  Future<ListaCompras?> findOne(int id) async {
    var result = await _dbConfiguration
        .execQuery('SELECT * FROM ListaCompras where Id = ?', [id]);
    return result.isEmpty ? null : ListaCompras.fromMap(result.first.fields);
  }

//
  @override
  Future<bool> update(ListaCompras value) async {
    var result = await _dbConfiguration.execQuery(
      'update ListaCompras set Total=?, dataLista=?, UsuarioId=?,ProdutoId=? where Id=?',
      [
        value.Total,
        value.dataLista,
        value.UsuarioId,
        value.ProdutoId,
        value.Id
      ],
    );
    return result.affectedRows > 0;
  }
}
