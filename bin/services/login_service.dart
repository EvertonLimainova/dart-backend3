import 'dart:developer';

import 'package:password_dart/password_dart.dart';

import '../to/auth_to.dart';
import 'usuario2_service.dart';
import 'usuario_service.dart';

class LoginService {
  final Usuario2Service _usuarioService;
  LoginService(this._usuarioService);

  Future<int> authenticate(AuthTO to) async {
    try {
      var user = await _usuarioService.findByEmail(to.email);
      print('User: ${user.toString()}');
      if (user == null) return -1;
      print(
          'password: ${to.password.toString()} passoard usuario: ${user.pass.toString()} usuarioID: ${user.Id}');      
      return Password.verify(to.password, user.pass!) ? user.Id! : -1;
    } catch (e) {
      log('[ERROR] -> in Authenticate method by email ${to.email}');
    }
    return -1;
  }
}
