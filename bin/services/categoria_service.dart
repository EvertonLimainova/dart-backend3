import '../dao/categoria_dao.dart';
import '../models/categoria_model.dart';
import 'generic_service.dart';

class CategoriaService implements GenericService<Categoria> {
  final CategoriaDAO _categoriaDAO;
  CategoriaService(this._categoriaDAO);
  @override
  Future<bool> delete(int id) async => _categoriaDAO.delete(id);

  @override
  Future<List<Categoria>> findAll() async => _categoriaDAO.findAll();

  @override
  Future<Categoria?> findOne(int id) async => _categoriaDAO.findOne(id);

  @override
  Future<bool> save(Categoria value) async {
    print('valor do id: ${value.Id.toString()}');
    if (value.Id != null && value.Id!>0) {
      return _categoriaDAO.update(value);
    } else {
      return _categoriaDAO.create(value);
    }
  }
}
