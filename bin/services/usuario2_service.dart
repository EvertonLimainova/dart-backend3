import '../dao/categoria_dao.dart';
import '../dao/usuario2_dao.dart';
import '../models/usuario2_model.dart';
import 'generic_service.dart';

class Usuario2Service implements GenericService<Usuario2>{
  final Usuario2DAO _usuario2DAO;
  Usuario2Service(this._usuario2DAO);
  @override
  Future<bool> delete(int id) async => _usuario2DAO.delete(id);

  @override
  Future<List<Usuario2>> findAll() async => _usuario2DAO.findAll();

  @override
  Future<Usuario2?> findOne(int id) async => _usuario2DAO.findOne(id);

  @override
  Future<bool> save(Usuario2 value) async {
    if (value.Id != null) {
      return _usuario2DAO.update(value);
    } else {     
      return _usuario2DAO.create(value);
    }
  }
  Future<Usuario2?> findByEmail(String email) async =>
      _usuario2DAO.findByEmail(email);

}