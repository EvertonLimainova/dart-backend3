import '../dao/produto_dao.dart';
import '../models/produto_model.dart';
import 'generic_service.dart';

class ProdutoService implements GenericService<Produto>{
  final ProdutoDAO _produtoDAO;
  ProdutoService(this._produtoDAO);
  @override
  Future<bool> delete(int id) async => _produtoDAO.delete(id);

  @override
  Future<List<Produto>> findAll() async => _produtoDAO.findAll();

  @override
  Future<Produto?> findOne(int id) async => _produtoDAO.findOne(id);

  @override
  Future<bool> save(Produto value) async {
    if (value.Id != null) {
      return _produtoDAO.update(value);
    } else {     
      return _produtoDAO.create(value);
    }
  }
}