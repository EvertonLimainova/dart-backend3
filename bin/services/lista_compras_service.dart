import '../dao/lista_compras_dao.dart';
import '../models/lista_compras_model.dart';
import 'generic_service.dart';

class ListaComprasService implements GenericService<ListaCompras>{
   final ListaComprasDAO _listacomprasDAO;
  ListaComprasService(this._listacomprasDAO);
  @override
  Future<bool> delete(int id) async => _listacomprasDAO.delete(id);

  @override
  Future<List<ListaCompras>> findAll() async => _listacomprasDAO.findAll();

  @override
  Future<ListaCompras?> findOne(int id) async => _listacomprasDAO.findOne(id);

  @override
  Future<bool> save(ListaCompras value) async {
    if (value.Id != null && value.Id!>0) {
      return _listacomprasDAO.update(value);
    } else {     
      return _listacomprasDAO.create(value);
    }
  }

}