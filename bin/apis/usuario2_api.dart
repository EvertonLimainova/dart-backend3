import 'dart:convert';

import 'package:shelf/shelf.dart';



import 'package:shelf_router/shelf_router.dart';

import '../models/usuario2_model.dart';
import '../services/generic_service.dart';
import 'api.dart';

class Usuario2Api extends Api{
 final GenericService<Usuario2> _service;
  Usuario2Api(this._service);

  @override
  Handler getHandler({
    List<Middleware>? middlewares,
    bool isSecurity = false,
  }) {
    Router router = Router();

    router.get('/usuario2', (Request req) async {
      String? id = req.url.queryParameters['id'];
      if (id == null) return Response(400);

      var usuario = await _service.findOne(int.parse(id));
      if (usuario == null) return Response(400);

      return Response.ok(jsonEncode(usuario.toJson()));
    });

    router.get('/usuario2', (Request req) async {
      List<Usuario2> usuarios = await _service.findAll();
      List<Map> noticiasMap = usuarios.map((e) => e.toJson()).toList();
      return Response.ok(jsonEncode(noticiasMap));
    });

    router.post('/usuario2', (Request req) async {
      var body = await req.readAsString();
      var result = await _service.save(
        Usuario2.fromRequest(jsonDecode(body)),
      );
      return result ? Response(201) : Response(500);
    });

    router.put('/usuario2', (Request req) async {
      var body = await req.readAsString();
     // print("variavel: ${NoticiaModel.fromRequest(jsonDecode(body))}");
      var result = await _service.save(
        Usuario2.fromRequest(jsonDecode(body)),
      );
      return result ? Response(201) : Response(500);
    });

    router.delete('/usuario2', (Request req) async {
      String? id = req.url.queryParameters['id'];
      if (id == null) return Response(400);

      var result = await _service.delete(int.parse(id));
      return result ? Response(200) : Response.internalServerError();
    });

    return createHandler(
      router: router,
      isSecurity: isSecurity,
      middlewares: middlewares,
    );
  }


}