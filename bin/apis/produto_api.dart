import 'dart:convert';

import 'package:shelf/shelf.dart';

import 'package:shelf_router/shelf_router.dart';

import '../models/produto_model.dart';
import '../services/generic_service.dart';
import 'api.dart';

class ProdutoAPi extends Api{
  final GenericService<Produto> _service;
  ProdutoAPi(this._service);

  @override
  Handler getHandler({
    List<Middleware>? middlewares,
    bool isSecurity = false,
  }) {
    Router router = Router();

    router.get('/produto', (Request req) async {
      String? id = req.url.queryParameters['id'];
      if (id == null) return Response(400);

      var usuario = await _service.findOne(int.parse(id));
      if (usuario == null) return Response(400);

      return Response.ok(jsonEncode(usuario.toJson()));
    });

    router.get('/produtos', (Request req) async {
      List<Produto> produtos = await _service.findAll();
      List<Map> produtoMap = produtos.map((e) => e.toJson()).toList();
      return Response.ok(jsonEncode(produtoMap));
    });

    router.post('/produto', (Request req) async {
      var body = await req.readAsString();
      var result = await _service.save(
        Produto.fromMap(jsonDecode(body)),
      );
      return result ? Response(201) : Response(500);
    });

    router.put('/produto', (Request req) async {
      var body = await req.readAsString();
     // print("variavel: ${NoticiaModel.fromRequest(jsonDecode(body))}");
      var result = await _service.save(
        Produto.fromMap(jsonDecode(body)),
      );
      return result ? Response(201) : Response(500);
    });

    router.delete('/produt', (Request req) async {
      String? id = req.url.queryParameters['id'];
      if (id == null) return Response(400);

      var result = await _service.delete(int.parse(id));
      return result ? Response(200) : Response.internalServerError();
    });

    return createHandler(
      router: router,
      isSecurity: isSecurity,
      middlewares: middlewares,
    );
  }

}