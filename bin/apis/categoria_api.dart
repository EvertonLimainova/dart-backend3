import 'dart:convert';

import 'package:shelf/shelf.dart';
import 'package:shelf/src/middleware.dart';

import 'package:shelf/src/handler.dart';
import 'package:shelf_router/shelf_router.dart';

import '../models/categoria_model.dart';
import '../services/categoria_service.dart';
import '../services/generic_service.dart';
import 'api.dart';

class CategoriaAPi extends Api {
  final GenericService<Categoria> _service;
  CategoriaAPi(this._service);

  @override
  Handler getHandler({
    List<Middleware>? middlewares,
    bool isSecurity = false,
  }) {
    Router router = Router();

    router.get('/categoria', (Request req) async {
      String? id = req.url.queryParameters['id'];
      if (id == null) return Response(400);

      var categoria = await _service.findOne(int.parse(id));
      if (categoria == null) return Response(400);

      return Response.ok(jsonEncode(categoria.toJson()));
    });

    router.get('/categorias', (Request req) async {
      List<Categoria> categorias = await _service.findAll();
      List<Map> categoriaMap = categorias.map((e) => e.toJson()).toList();
      return Response.ok(jsonEncode(categoriaMap));
    });

    router.post('/categoria', (Request req) async {
      var body = await req.readAsString();
      print('${jsonDecode(body)}');
      var result = await _service.save(
        Categoria.fromMap(jsonDecode(body)),
      );
      return result ? Response(201) : Response(500);
    });

    router.put('/categoria', (Request req) async {
      var body = await req.readAsString();
      print('teste put: ${body}');
      // print("variavel: ${NoticiaModel.fromRequest(jsonDecode(body))}");
      var result = await _service.save(
        Categoria.fromMap(jsonDecode(body)),
      );
      return result ? Response(200) : Response(500);
    });

    router.delete('/categoria', (Request req) async {
      String? id = req.url.queryParameters['Id'];
      if (id == null) return Response(400);

      var result = await _service.delete(int.parse(id));
      return result ? Response(201) : Response.internalServerError();
    });

    return createHandler(
      router: router,
      isSecurity: isSecurity,
      middlewares: middlewares,
    );
  }
}
