import 'dart:convert';

import 'package:shelf/shelf.dart';

import 'package:shelf_router/shelf_router.dart';

import '../models/lista_compras_model.dart';
import '../services/generic_service.dart';
import 'api.dart';

class ListaComprasAPI extends Api{
   final GenericService<ListaCompras> _service;
  ListaComprasAPI(this._service);

  @override
  Handler getHandler({
    List<Middleware>? middlewares,
    bool isSecurity = false,
  }) {
    Router router = Router();

    router.get('/listacompra', (Request req) async {
      String? id = req.url.queryParameters['id'];
      if (id == null) return Response(400);

      var listacompras = await _service.findOne(int.parse(id));
      if (listacompras == null) return Response(400);

      return Response.ok(jsonEncode(listacompras.toJson()));
    });

    router.get('/listacompras', (Request req) async {
      List<ListaCompras> listacompras = await _service.findAll();
      List<Map> listacomprasMap = listacompras.map((e) => e.toJson()).toList();
      return Response.ok(jsonEncode(listacomprasMap));
    });

    router.post('/listacompra', (Request req) async {
      var body = await req.readAsString();
      var result = await _service.save(
        ListaCompras.fromMap(jsonDecode(body)),
      );
      return result ? Response(201) : Response(500);
    });

    router.put('/listacompra', (Request req) async {
      var body = await req.readAsString();
     // print("variavel: ${NoticiaModel.fromRequest(jsonDecode(body))}");
      var result = await _service.save(
        ListaCompras.fromMap(jsonDecode(body)),
      );
      return result ? Response(201) : Response(500);
    });

    router.delete('/listacompra', (Request req) async {
      String? id = req.url.queryParameters['id'];
      if (id == null) return Response(400);

      var result = await _service.delete(int.parse(id));
      return result ? Response(200) : Response.internalServerError();
    });

    return createHandler(
      router: router,
      isSecurity: isSecurity,
      middlewares: middlewares,
    );
  }

}