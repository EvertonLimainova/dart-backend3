import 'package:shelf/shelf.dart';

import 'apis/categoria_api.dart';
import 'apis/lista_comras.api.dart';
import 'apis/noticias_api.dart';
import 'apis/login_api.dart';
import 'apis/produto_api.dart';
import 'apis/usuario2_api.dart';
import 'apis/usuario_api.dart';
import 'infra/custom_server.dart';
import 'infra/dependency_injector/injects.dart';
import 'infra/middleware_interception.dart';
import 'utils/custom_env.dart';

void main() async {
  //CustomEnv.fromFile('.env-dev');

  final _di = Injects.initialize();

  var cascadeHandler = Cascade()
      .add(_di.get<LoginApi>().getHandler())
      .add(_di.get<NoticiasApi>().getHandler(isSecurity: false))
      //.add(_di.get<UsuarioApi>().getHandler(isSecurity: true))
      .add(_di.get<Usuario2Api>().getHandler(isSecurity: true))
      .add(_di.get<ProdutoAPi>().getHandler(isSecurity: true))
      .add(_di.get<CategoriaAPi>().getHandler(isSecurity: true))
      .add(_di.get<ListaComprasAPI>().getHandler(isSecurity: true))
      .handler;

  var handler = Pipeline()
      .addMiddleware(logRequests()) // global Middlewares
      .addMiddleware(MInterception.contentTypeJson) // global Middlewares
      .addMiddleware(MInterception.cors) // global Middlewares
      .addHandler(cascadeHandler);

  await CustomServer().initialize(
    handler: handler,
    address: 'localhost',//await CustomEnv.get<String>(key: 'server_address'),
    port: 502,//await CustomEnv.get<int>(key: 'server_port'),
  );
}
